
from django.conf.urls import url
from . import views
from .forms import TodoForm
from .models import Todo

print("inside url")
urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'^add/', views.addTodo, name='add'),
	url(r'^complete/<todo_id>/', views.completeTodo, name='complete'),
	url(r'^deletecomplete/', views.deleteCompleted, name='deletecomplete'),
	url(r'^deleteall', views.deleteAll, name='deleteall')
]